/**
 * 
 */
package org.luismmribeiro.tutorials.jdk8plus;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>Class that provides examples of the usage of new methods in the Standard Class
 * Library.</p>
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class NewMethodsOnSCL {
    public void newMethodOptionalIfPresentOrElse(Optional<String> optionalString) {
        // Before we should write:
        if(optionalString.isPresent()) {
            System.out.println("The string is present: " + optionalString.get());
        }
        else {
            System.out.println("No string available");
        }
        
        // Now we can write:
        optionalString.ifPresentOrElse(
                s -> System.out.println("The string is present: " + s),
                () -> System.out.println("No string available"));
    }
    
    public Optional<String> newMethodOptionalOr(Optional<String> optionalString) {
        // Before we should write:
        Optional<String> safeOptional;
        if(optionalString.isPresent()) {
            safeOptional = optionalString;
        }
        else {
            safeOptional = Optional.of("Empty String");
        }
        
        // Now we can write:
        safeOptional = optionalString.or(() -> Optional.of("Empty String"));
        
        return safeOptional;
    }
    
    public List<String> newMethodOptionalStream(Optional<String> optionalString) {
        return optionalString.stream().collect(Collectors.toList());
    }
    
    @SuppressWarnings("unused")
    public void newImmutableCollectionsMethod() {
        Set<String> immutableSet = null;
        // Before we should write:
        Set<String> names = new HashSet<>();
        names.add("John");
        names.add("George");
        names.add("Betty");
        immutableSet = Collections.unmodifiableSet(names);
        
        // Now we can write:
        immutableSet = Set.of("John", "George", "Betty");
        
        // For Lists:
        List<String> immutableList = List.of("John", "George", "Betty");
        
        // For Maps:
        Map<String, Long> immutableMap = Map.of("John", 1L, "George", 2L);
    }
}
