package org.luismmribeiro.tutorials.jdk8plus;

import java.time.DayOfWeek;
import java.util.List;
import java.util.function.Predicate;

/**
 * Interface that exemplifies private methods on interfaces.
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public interface PrivateMethodsOnInterfaces {
    default long countWorkingDays(List<DayOfWeek> daysOfWeek) {
        return countWeekDays(daysOfWeek,
                             d -> d != DayOfWeek.SATURDAY && d != DayOfWeek.SUNDAY);
    }
    
    default long countWeekendDays(List<DayOfWeek> daysOfWeek) {
        return countWeekDays(daysOfWeek,
                             d -> d == DayOfWeek.SATURDAY || d == DayOfWeek.SUNDAY);
    }
    
    private long countWeekDays(List<DayOfWeek> daysOfWeek,
                               Predicate<DayOfWeek> predicate) {
        return daysOfWeek.stream()
                .filter(predicate)
                .count();
    }
}
