package org.luismmribeiro.tutorials.jdk8plus;

import java.time.DayOfWeek;
import java.util.Random;
import java.util.function.IntBinaryOperator;

/**
 * <p>Class that provides examples of the usage of <code>var</code> type name.</p>
 * <p><code>var</code> is a reserved type name, not a keyword, which means that existing
 * code that uses <code>var</code> as a variable, method, or package name is not
 * affected. However, code that uses <code>var</code> as a class or interface name is
 * affected and the class or interface needs to be renamed.</p> 
 * 
 * @author Luis Ribeiro <luismmribeiro@gmail.com>
 */
public class VarTypeName {
    private IntBinaryOperator randomCalculator =
            (var minValue, var maxValue) -> {

//            (var minValue, int maxValue) -> { // var can not be mixed with non-var parameters
//            (var minValue, maxValue) -> { // var can not be mixed with implicit type parameters

                var random = new Random();
                return random.ints(minValue, maxValue + 1)
                        .limit(1)
                        .findFirst()
                        .getAsInt();
            };

    public static void main(String[] args) {
        var v = new VarTypeName();

        for(var i = 0; i < 10; i++) {
            v.getWeekdayLettersCount();
        }
    }

    public void getWeekdayLettersCount() {
        var message = "%s has %s letters";

//        For instance: we can't assign int values to a variable already defined as
//        String (event using the var reserved type name):
//        message = 556;

        var randomDay = getRandomWeekDay();
        var numberOfLetters = countNumberOfLetters(randomDay);
        System.out.println(String.format(message, randomDay.name(), numberOfLetters));
    }

    public void getWeekdayLettersCountWithOriginalTypes() {
        String message = "%s has %s letters";
        DayOfWeek randomDay = getRandomWeekDay();
        int numberOfLetters = countNumberOfLetters(randomDay);
        System.out.println(String.format(message, randomDay.name(), numberOfLetters));
    }

    public DayOfWeek getRandomWeekDay() {
        return DayOfWeek.values()[randomCalculator.applyAsInt(0, 6)];
    }

    public int countNumberOfLetters(DayOfWeek day) {
//    The method parameters do not allow using var. Otherwise the compiler wouldn't be
//    capable of inferring the variable type.
//    public int countNumberOfLetters(var day) {
        return day.name().length();
    }
}
